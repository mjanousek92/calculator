package com.janousek.calculator.expression;

import com.janousek.calculator.evaluator.ExpressionEvaluator;
import com.janousek.calculator.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by martin on 17.2.18.
 */
public class ExpressionEvaluatorTest {

	private Double[] doubleValues;

	@Before
	public void initConstants(){
		doubleValues = new Double[]{0.0, -91.0, 625.49, -1954.001};
	}

	@Test
	public void evaluateOnlyApplyExpressionTest() {
		ConstantExpression applyExpression = new ConstantExpression(doubleValues[0]);
		ExpressionEvaluator ev = new ExpressionEvaluator(applyExpression, new LinkedList<>());

		Assert.assertTrue("Evaluator with only apply expression has result " + doubleValues[0],
				Utils.hasEqualValue(ev.evaluate(), doubleValues[0]));
	}

	@Test
	public void evaluateAdditionExpressionTest() {
		List<BinaryExpression> expressions = new LinkedList<>();
		ConstantExpression applyExpression = new ConstantExpression(doubleValues[0]);
		expressions.add(new AdditionExpression(doubleValues[1]));
		ExpressionEvaluator ev = new ExpressionEvaluator(applyExpression, expressions);

		Double expectedResult = doubleValues[1] + doubleValues[0];
		Assert.assertTrue("Evaluator produces result " + expectedResult,
				Utils.hasEqualValue(ev.evaluate(), expectedResult));
	}

	@Test
	public void evaluateExampleOneTest() {
		List<BinaryExpression> expressions = new LinkedList<>();
		ConstantExpression applyExpression = new ConstantExpression(4.0);
		expressions.add(new AdditionExpression(2.0));
		expressions.add(new MultiplicationExpression(3.0));
		ExpressionEvaluator ev = new ExpressionEvaluator(applyExpression, expressions);

		Double expectedResult = (4.0 + 2.0) * 3.0;
		Assert.assertTrue("Evaluator produces result " + expectedResult,
				Utils.hasEqualValue(ev.evaluate(), expectedResult));
	}


	@Test
	public void evaluateMoreExpressionsTest() {
		List<BinaryExpression> expressions = new LinkedList<>();
		ConstantExpression applyExpression = new ConstantExpression(9.0);
		expressions.add(new AdditionExpression(5.0));
		expressions.add(new MultiplicationExpression(3.0));
		expressions.add(new SubtractionExpression(10.0));
		expressions.add(new DivisionExpression(2.0));
		ExpressionEvaluator ev = new ExpressionEvaluator(applyExpression, expressions);

		Double expectedResult = (((9.0 + 5.0) * 3.0) - 10.0) / 2.0;
		Assert.assertTrue("Evaluator produces result " + expectedResult,
				Utils.hasEqualValue(ev.evaluate(), expectedResult));
	}
}