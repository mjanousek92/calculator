package com.janousek.calculator.expression;

import com.janousek.calculator.exception.DivisionByZeroException;
import com.janousek.calculator.exception.MissingLeftExpressionException;
import com.janousek.calculator.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by martin on 15.2.18.
 */
public class ExpressionTest {

	private Double[] doubleValues;

	@Before
	public void initConstants(){
		doubleValues = new Double[]{-91.0, 625.49, -1954.001, 0.0};
	}

	@Test
	public void createExpressionTest() {
		Expression applyExpression = new ConstantExpression(0.0);
		Expression additionExpression = new AdditionExpression(0.0);
		Expression subtractionExpression = new SubtractionExpression(0.0);
		Expression multiplicationExpression = new MultiplicationExpression(0.0);
		Expression divisionExpression = new DivisionExpression(0.0);
	}

	@Test
	public void evaluateConstantExpressionsTest() {
		ConstantExpression[] constants = createConstantExpressions();

		for (int i = 0; i < doubleValues.length; i++) {
			Assert.assertTrue("ConstantExpression has value " + doubleValues[i], Utils.hasEqualValue(constants[i], doubleValues[i]));
		}
	}

	@Test(expected = MissingLeftExpressionException.class)
	public void evaluateExpressionWithMissingLeftExpressionTest() {
		AdditionExpression divisionExpression = new AdditionExpression(5.0);
		divisionExpression.evaluate();
	}

	@Test
	public void evaluateAdditionExpressionsTest() {
		AdditionExpression[] additionExpressions = createAdditionExpressions();

		for (int i = 0; i < doubleValues.length - 1; i++) {
			Double expectedResult = doubleValues[i + 1] + doubleValues[i];
			Assert.assertTrue("AdditionExpression has value " + expectedResult,
					Utils.hasEqualValue(additionExpressions[i].evaluate(), expectedResult));
		}
	}

	@Test
	public void evaluateSubtractionExpressionsTest() {
		SubtractionExpression[] subtractionExpressions = createSubtractionExpressions();

		for (int i = 0; i < doubleValues.length - 1; i++) {
			Double expectedResult = doubleValues[i + 1] - doubleValues[i];
			Assert.assertTrue("AdditionExpression has value " + expectedResult,
					Utils.hasEqualValue(subtractionExpressions[i].evaluate(), expectedResult));
		}
	}

	@Test
	public void evaluateMultiplicationExpressionsTest() {
		MultiplicationExpression[] multiplicationExpressions = createMultiplicationExpressions();

		for (int i = 0; i < doubleValues.length - 1; i++) {
			Double expectedResult = doubleValues[i + 1] * doubleValues[i];
			Assert.assertTrue("AdditionExpression has value " + expectedResult,
					Utils.hasEqualValue(multiplicationExpressions[i].evaluate(), expectedResult));
		}
	}

	@Test
	public void evaluateDivisionExpressionsTest() {
		DivisionExpression[] divisionExpression = createDivisionExpressions();

		for (int i = 0; i < doubleValues.length - 1; i++) {
			Double expectedResult = doubleValues[i + 1] / doubleValues[i];
			Assert.assertTrue("AdditionExpression has value " + expectedResult,
					Utils.hasEqualValue(divisionExpression[i].evaluate(), expectedResult));
		}
	}

	@Test(expected = DivisionByZeroException.class)
	public void evaluateDivisionByZeroExpressionsTest() {
		DivisionExpression divisionExpression = new DivisionExpression(0.0);
		divisionExpression.setLeftOperand(5.0);
		divisionExpression.evaluate();
	}

	private ConstantExpression[] createConstantExpressions() {
		ConstantExpression[] constantExpressions = new ConstantExpression[doubleValues.length];

		for (int i = 0; i < doubleValues.length; i++) {
			constantExpressions[i] = new ConstantExpression(doubleValues[i]);
		}
		return constantExpressions;
	}

	private AdditionExpression[] createAdditionExpressions() {
		AdditionExpression[] additionExpressions = new AdditionExpression[doubleValues.length - 1];

		for (int i = 0; i < doubleValues.length - 1; i++) {
			AdditionExpression e = new AdditionExpression(doubleValues[i]);
			e.setLeftOperand(doubleValues[i + 1]);
			additionExpressions[i] = e;
		}
		return additionExpressions;
	}

	private SubtractionExpression[] createSubtractionExpressions() {
		SubtractionExpression[] subtractionExpressions = new SubtractionExpression[doubleValues.length - 1];

		for (int i = 0; i < doubleValues.length - 1; i++) {
			SubtractionExpression e = new SubtractionExpression(doubleValues[i]);
			e.setLeftOperand(doubleValues[i + 1]);
			subtractionExpressions[i] = e;
		}
		return subtractionExpressions;
	}

	private MultiplicationExpression[] createMultiplicationExpressions() {
		MultiplicationExpression[] multiplicationExpressions = new MultiplicationExpression[doubleValues.length - 1];

		for (int i = 0; i < doubleValues.length - 1; i++) {
			MultiplicationExpression e = new MultiplicationExpression(doubleValues[i]);
			e.setLeftOperand(doubleValues[i + 1]);
			multiplicationExpressions[i] = e;
		}
		return multiplicationExpressions;
	}

	private DivisionExpression[] createDivisionExpressions() {
		DivisionExpression[] divisionExpression = new DivisionExpression[doubleValues.length - 1];

		for (int i = 0; i < doubleValues.length - 1; i++) {
			DivisionExpression e = new DivisionExpression(doubleValues[i]);
			e.setLeftOperand(doubleValues[i + 1]);
			divisionExpression[i] = e;
		}
		return divisionExpression;
	}
}