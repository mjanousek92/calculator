package com.janousek.calculator.parser;

import com.janousek.calculator.expression.*;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by martin on 17.2.18.
 */
public class ExpressionParserTest {

	@Test
	public void parseApplyCommandTest(){
		ConstantExpression original = new ConstantExpression(5.0);
		ConstantExpression generated = (ConstantExpression) ExpressionParser.parse("apply 5");

		Assert.assertTrue("Parse apply command", original.equals(generated));
	}

	@Test
	public void parseAddCommandTest(){
		AdditionExpression original = new AdditionExpression(5.0);
		AdditionExpression generated = (AdditionExpression) ExpressionParser.parse("add 5");

		Assert.assertTrue("Parse add command", original.equals(generated));
	}

	@Test
	public void parseSubCommandTest(){
		SubtractionExpression original = new SubtractionExpression(5.0);
		SubtractionExpression generated = (SubtractionExpression) ExpressionParser.parse("sub 5");

		Assert.assertTrue("Parse sub command", original.equals(generated));
	}

	@Test
	public void parseMultiplyCommandTest(){
		MultiplicationExpression original = new MultiplicationExpression(5.0);
		MultiplicationExpression generated = (MultiplicationExpression) ExpressionParser.parse("multiply 5");

		Assert.assertTrue("Parse multiply command", original.equals(generated));
	}

	@Test
	public void parseDivideCommandTest(){
		DivisionExpression original = new DivisionExpression(5.0);
		DivisionExpression generated = (DivisionExpression) ExpressionParser.parse("divide 5");

		Assert.assertTrue("Parse divide command", original.equals(generated));
	}
}