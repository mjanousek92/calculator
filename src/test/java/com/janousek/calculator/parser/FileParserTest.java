package com.janousek.calculator.parser;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by martin on 17.2.18.
 */
public class FileParserTest {

	public static final String INPUT_FILE = "Input.txt";
	private static final String NON_EXIST_FILE = "NonExistFile.txt";

	@Test
	public void getLinesFromFileTest(){
		String path = getClass().getClassLoader().getResource(INPUT_FILE).getPath();
		List<String> lines = FileParser.parseLines(path);
		Assert.assertEquals("Load and parse file to lines.", lines.size(), 2);
	}

	@Test(expected = RuntimeException.class)
	public void getNonExistFileTest(){
		List<String> lines = FileParser.parseLines(NON_EXIST_FILE);
		Assert.assertEquals("Load and parse file to lines.", lines.size(), 2);
	}
}