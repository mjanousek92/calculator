package com.janousek.calculator;

import org.junit.Test;

/**
 * Created by martin on 17.2.18.
 */
public class CalculatorTest {

	public static final String INPUT_FILE_EXAMPLE_1 = "Ex1.txt";

	@Test
	public void startCalculatorTest(){
		String path = getClass().getClassLoader().getResource(INPUT_FILE_EXAMPLE_1).getPath();
		Calculator calculator = new Calculator(path);
		calculator.start();
	}

}