package com.janousek.calculator.expression;

import com.janousek.calculator.exception.DivisionByZeroException;
import com.janousek.calculator.utils.Utils;

/**
 * Created by martin on 15.2.18.
 */
public class DivisionExpression extends BinaryExpression {
	public DivisionExpression(Double operand) {
		super(operand);
	}

	@Override
	public Double evaluate() {
		checkMissingLeftOperand();
		if(Utils.hasEqualValue(operand, 0.0)) {
			throw createDivisionByZeroException();
		} else {
			return leftOperand / operand;
		}
	}

	private DivisionByZeroException createDivisionByZeroException() {
		return new DivisionByZeroException(String.format("Expression: %.2f / 0", leftOperand));
	}

}
