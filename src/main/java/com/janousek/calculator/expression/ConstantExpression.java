package com.janousek.calculator.expression;

/**
 * Created by martin on 15.2.18.
 */
public class ConstantExpression extends Expression {

	public ConstantExpression(Double operand) {
		super(operand);
	}

	public Double evaluate() {
		return operand;
	}
}
