package com.janousek.calculator.expression;

/**
 * Created by martin on 15.2.18.
 */
public class SubtractionExpression extends BinaryExpression {

	public SubtractionExpression(Double operand) {
		super(operand);
	}

	@Override
	public Double evaluate() {
		checkMissingLeftOperand();
		return leftOperand - operand;
	}
}
