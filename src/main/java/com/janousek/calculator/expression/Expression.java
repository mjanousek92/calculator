package com.janousek.calculator.expression;

/**
 * Created by martin on 15.2.18.
 */
public abstract class Expression {

	protected Double operand;

	protected Expression(Double operand) {
		this.operand = operand;
	}

	public abstract Double evaluate();

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Expression)) return false;

		Expression that = (Expression) o;

		return operand != null ? operand.equals(that.operand) : that.operand == null;

	}

	public enum Type{
		APPLY("apply"), ADD("add"), SUB("sub"), MULTIPLY("multiply"), DIVIDE("divide");

		private final String name;

		Type(String name) {
			this.name = name;
		}
	}
}
