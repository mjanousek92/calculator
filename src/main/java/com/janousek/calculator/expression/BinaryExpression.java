package com.janousek.calculator.expression;

import com.janousek.calculator.exception.MissingLeftExpressionException;

/**
 * Created by martin on 15.2.18.
 */
public abstract class BinaryExpression extends Expression {

	protected Double leftOperand;

	public BinaryExpression(Double rightOperand) {
		super(rightOperand);
	}

	public void setLeftOperand(Double leftOperand) {
		this.leftOperand = leftOperand;
	}

	protected MissingLeftExpressionException createMissiningLeftExpressionException() {
		return new MissingLeftExpressionException(String.format("The left expression missing."));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof BinaryExpression)) return false;

		BinaryExpression that = (BinaryExpression) o;

		if (leftOperand != null ? !leftOperand.equals(that.leftOperand) : that.leftOperand != null) return false;
		return operand.equals(that.operand);

	}

	protected void checkMissingLeftOperand() {
		if(leftOperand == null) {
			throw createMissiningLeftExpressionException();
		}
	}
}
