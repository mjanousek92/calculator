package com.janousek.calculator.expression;

/**
 * Created by martin on 15.2.18.
 */
public class AdditionExpression extends BinaryExpression {

	public AdditionExpression(Double operand) {
		super(operand);
	}

	@Override
	public Double evaluate() {
		checkMissingLeftOperand();
		return leftOperand + operand;
	}
}
