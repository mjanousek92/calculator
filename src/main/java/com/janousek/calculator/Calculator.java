package com.janousek.calculator;

import com.janousek.calculator.evaluator.ExpressionEvaluator;
import com.janousek.calculator.expression.BinaryExpression;
import com.janousek.calculator.expression.ConstantExpression;
import com.janousek.calculator.parser.ExpressionParser;
import com.janousek.calculator.parser.FileParser;

import java.util.LinkedList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class Calculator
{

	private final String pathToInputFile;

	public Calculator(String pathToInputFile) {
		this.pathToInputFile = pathToInputFile;
	}

	public void start() {
		ExpressionEvaluator evaluator = createEvaluatorInstance(pathToInputFile);
		Double result = evaluator.evaluate();
		printResult(result);
	}

	private ExpressionEvaluator createEvaluatorInstance(String pathToInputFile) {
		List<String> lines = FileParser.parseLines(pathToInputFile);
		return createEvaluator(lines);
	}

	private ExpressionEvaluator createEvaluator(List<String> lines) {
		ConstantExpression applyExpression = null;
		List<BinaryExpression> expressions = new LinkedList<>();

		for (int i = 0; i < lines.size(); i++) {
			if(isLast(lines, i)) {
				applyExpression = (ConstantExpression) ExpressionParser.parse(lines.get(i));
			} else {
				expressions.add((BinaryExpression) ExpressionParser.parse(lines.get(i)));
			}
		}
		return new ExpressionEvaluator(applyExpression, expressions);
	}

	private void printResult(Double result) {
		System.out.println(Math.round(result));
	}

	private boolean isLast(List<String> lines, int i) {
		return i == lines.size() - 1;
	}
}
