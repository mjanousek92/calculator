package com.janousek.calculator.factory;

import com.janousek.calculator.expression.*;

/**
 * Created by martin on 17.2.18.
 */
public class ExpressionFactory {
	private static final int INDEX_OF_OPERATION = 0;
	private static final int INDEX_OF_OPERAND = 1;

	public static Expression createExpression(String line) {
		String[] words = line.split(" ");
		Expression.Type operator = Expression.Type.valueOf(words[INDEX_OF_OPERATION].toUpperCase());
		Double operand = new Double(words[INDEX_OF_OPERAND]);

		return createOperation(operator, operand);
	}

	private static Expression createOperation(Expression.Type operator, Double operand) {
		switch (operator) {
			case APPLY:
				return new ConstantExpression(operand);
			case ADD:
				return new AdditionExpression(operand);
			case SUB:
				return new SubtractionExpression(operand);
			case MULTIPLY:
				return new MultiplicationExpression(operand);
			case DIVIDE:
				return new DivisionExpression(operand);
			default:
				throw new UnsupportedOperationException(String.format("Operator %s is not supported.", operator));
		}
	}
}
