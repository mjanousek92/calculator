package com.janousek.calculator.utils;

import com.janousek.calculator.expression.ConstantExpression;

/**
 * Created by martin on 17.2.18.
 */
public class Utils {

	public static boolean hasEqualValue(ConstantExpression constant, Double constantValue) {
		return hasEqualValue(constant.evaluate(), constantValue);
	}

	public static boolean hasEqualValue(Double firstValue, Double secondValue) {
		return Double.compare(firstValue, secondValue) == 0;
	}
}
