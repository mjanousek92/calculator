package com.janousek.calculator.evaluator;

import com.janousek.calculator.expression.BinaryExpression;
import com.janousek.calculator.expression.ConstantExpression;

import java.util.List;

/**
 * Created by martin on 17.2.18.
 */
public class ExpressionEvaluator {

	private ConstantExpression applyExpression;
	private List<BinaryExpression> expressions;

	public ExpressionEvaluator(ConstantExpression applyExpression, List<BinaryExpression> expressions)
	{
		this.applyExpression = applyExpression;
		this.expressions = expressions;
	}

	public Double evaluate() {
		Double result = applyExpression.evaluate();
		for (BinaryExpression e : expressions) {
			e.setLeftOperand(result);
			result = e.evaluate();
		}
		return result;
	}
}
