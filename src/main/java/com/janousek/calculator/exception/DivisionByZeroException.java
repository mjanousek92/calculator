package com.janousek.calculator.exception;

/**
 * Created by martin on 17.2.18.
 */
public class DivisionByZeroException extends RuntimeException {
	public DivisionByZeroException(String message) {
		super(message);
	}
}
