package com.janousek.calculator.exception;

/**
 * Created by martin on 17.2.18.
 */
public class MissingLeftExpressionException extends RuntimeException {
	public MissingLeftExpressionException(String message) {
		super(message);
	}
}
