package com.janousek.calculator.parser;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * Created by martin on 17.2.18.
 */
public class FileParser {
	public static List<String> parseLines(String pathToFile) {
		Path path = new File(pathToFile).toPath();
		try {
			return Files.readAllLines(path);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
