package com.janousek.calculator.parser;

import com.janousek.calculator.expression.Expression;
import com.janousek.calculator.factory.ExpressionFactory;

/**
 * Created by martin on 17.2.18.
 */
public class ExpressionParser {
	public static Expression parse(String line) {
		return ExpressionFactory.createExpression(line);
	}
}
