package com.janousek.calculator;

/**
 * Created by martin on 17.2.18.
 */
public class Application {

	public static void main(String[] args) {
		if(args.length == 1) {
			startCalculator(args[0]);
		} else {
			printErrorFewArguments();
		}
	}

	private static void startCalculator(String pathToInputFile) {
		Calculator calculator = new Calculator(pathToInputFile);
		calculator.start();
	}

	private static void printErrorFewArguments() {
		System.out.println("Program requires 1 argument (path to input file).");
	}
}
